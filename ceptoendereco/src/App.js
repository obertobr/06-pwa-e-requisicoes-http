import React, {useState}  from "react";
import Endereco from "./Components/endereco";
import CEP from "./Components/CEP";
import OfflineTest from "./Components/OfflineTest"
import './App.css';

function App() {
  const [cep, setCep] =useState("")
  const [endereco, setEndereco] =useState("")

  const [latitude, setLatitude] =useState(-22.952022552490234)
  const [longitude, setLongitude] =useState(-46.5418586730957)
  
  return (
    <div className="body">
      <OfflineTest/>
      <Endereco endereco={endereco} cep={setCep} setEndereco={setEndereco} latitude={latitude} setLatitude={setLatitude} longitude={longitude} setLongitude={setLongitude}/>
      <div className="meio">
        <p>↔</p>
      </div>
      <CEP cep={cep} setCEP={setCep} setEndereco={setEndereco} setLatitude={setLatitude} setLongitude={setLongitude}/>
    </div>
  );
}

export default App;
