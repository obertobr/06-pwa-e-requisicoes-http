import React from "react";

function CEP(props) {
  
  return (
    <div className="card">
        <span className="titulo">CEP:</span>
        <input className="input" maxLength={9} value={props.cep} onKeyDown={(e) => {
            if(e.key.match(/^[0-9-]/)){
                props.setCEP(props.cep + e.key)
            } else if(e.key == "Backspace") {
                props.setCEP(props.cep.slice(0, -1));
            }
            if((e.target.value + e.key).length > 7 && (e.target.value + e.key).length < 10 && (e.target.value + e.key).replace("-","").match(/^[0-9-]/)) {
                fetch("http://viacep.com.br/ws/"+(e.target.value + e.key).replace("-","")+"/json/")
                .then(function(response) {
                    response.json().then(function(data) {
                        props.setEndereco(data["uf"]+", "+data["localidade"]+", "+data["bairro"]+", "+data["logradouro"])
                        fetch("http://dev.virtualearth.net/REST/v1/Locations/"+e.target.value+"?o=json&maxResults=1&key=Ajmc010ZenFBHt7-lNqKT-aN3m67Yt7p8CG-X9fYbl4HcOaC8DRMU2e_JgJJQC7t")
                        .then(function(response) {
                            response.json().then(function(data) {
                                props.setLatitude(data["resourceSets"][0]["resources"][0]["geocodePoints"][0]["coordinates"][0]);
                                props.setLongitude(data["resourceSets"][0]["resources"][0]["geocodePoints"][0]["coordinates"][1]);
                                if(data["resourceSets"][0]["resources"][0]["address"]["postalCode"] != null){
                                    props.cep(data["resourceSets"][0]["resources"][0]["address"]["postalCode"]);
                                } else {
                                    props.cep("");
                                }
                            });
                        })
                        .catch(function(err) {
                            console.error('Failed retrieving information', err);
                        });
                    });
                })
                .catch(function(err) {
                    console.error('Failed retrieving information', err);
                });
            }
        }}></input>
    </div>
  );
}

export default CEP;
