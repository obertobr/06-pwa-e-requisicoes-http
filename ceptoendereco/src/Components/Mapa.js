import React from "react";
import BingMapsReact from "bingmaps-react";

function Mapa(props) {

  return (
    <div className="map">
      <BingMapsReact
      bingMapsKey="Ajmc010ZenFBHt7-lNqKT-aN3m67Yt7p8CG-X9fYbl4HcOaC8DRMU2e_JgJJQC7t"
      pushPins={props.pushPins}
      height="100%"
      mapOptions={{
        navigationBarMode: "square",
      }}
      width="100%"
      viewOptions={{
        center: { latitude: props.latitude, longitude: props.longitude },
        zoom: 18.8
      }}
    />
    </div>
  );
}

export default Mapa;
