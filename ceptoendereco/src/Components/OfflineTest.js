import { useState, useEffect } from 'react';

function PWAOfflineStatus(props) {
    const [isOnline, setOnlineStatus] = useState(true);

    useEffect(() => {
        const setFromEvent = function(event) {
            if(event.type === 'online') {
                setOnlineStatus(true);
            }
            else if(event.type === 'offline') {
                setOnlineStatus(false);
            }
            
        }

        window.addEventListener("online", setFromEvent);
        window.addEventListener("offline", setFromEvent);
        
        return() => {
            window.removeEventListener("online", setFromEvent);
            window.removeEventListener("offline", setFromEvent);
        }
    });

    if(!isOnline){alert("Você esta Offline")}
};

export default PWAOfflineStatus;