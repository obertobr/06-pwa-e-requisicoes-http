import React, {useState} from "react";
import Mapa from "./Mapa";

function Endereco(props) {

  const pushPin = {
    center: {
      latitude: props.latitude,
      longitude: props.longitude,
    }
  }

  const pushPins = [pushPin];
  
  return (
    <div className="card">
        <span className="titulo">Endereço:</span>
        <input className="input" value={props.endereco} onChange={(e) => {
            fetch("http://dev.virtualearth.net/REST/v1/Locations/"+e.target.value+"?o=json&maxResults=1&key=Ajmc010ZenFBHt7-lNqKT-aN3m67Yt7p8CG-X9fYbl4HcOaC8DRMU2e_JgJJQC7t")
            .then(function(response) {
                response.json().then(function(data) {
                    props.setLatitude(data["resourceSets"][0]["resources"][0]["geocodePoints"][0]["coordinates"][0]);
                    props.setLongitude(data["resourceSets"][0]["resources"][0]["geocodePoints"][0]["coordinates"][1]);
                    if(data["resourceSets"][0]["resources"][0]["address"]["postalCode"] != null){
                        props.cep(data["resourceSets"][0]["resources"][0]["address"]["postalCode"]);
                    } else {
                        props.cep("");
                    }
                });
            })
            .catch(function(err) {
                console.error('Failed retrieving information', err);
            });
        }} onKeyDown={(e) => {
            if(e.key.match(/([A-Za-z0-9-., ç])/) && e.key.length==1){
                props.setEndereco(e.target.value + e.key)
            } else if(e.key == "Backspace") {
                props.setEndereco(e.target.value.slice(0, -1))
            }
        }}></input>
        <Mapa latitude={props.latitude} longitude={props.longitude} pushPins={pushPins}/>
    </div>
  );
}

export default Endereco;
